#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>

#define MAX_LENGTH 10  24

char *path;
int hour, minute, second;

void error(char *message)
{
    printf("error: %s\n", message);
    exit(EXIT_FAILURE);
}

// mem-parse argumen yang diberikan pada command line. Fungsi ini menerima dua parameter
void parse_arguments(int argc, char **argv)
{
    // jumlah argumen
    if (argc != 5)
    {
        error("incorrect number of arguments");
    }

    // Parse hour argument
    if (strcmp(argv[1], "*") == 0)
    {
        hour = -1;
    }
    else
    {
        // cek apakah input hanya terdiri dari angka
        for (int i = 0; i < strlen(argv[1]); i++)
        {
            if (!isdigit(argv[1][i]))
            {
                error("hour argument must be numeric");
            }
        }

        // dikonversi bilbul atoi
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23)
        {
            error("hour argument must be between 0 and 23");
        }
    }

    // Parse minute argument
    for (int i = 0; i < strlen(argv[2]); i++)
    {
        if (!isdigit(argv[2][i]))
        {
            error("minute argument must be numeric");
        }
    }
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59)
    {
        error("minute argument must be between 0 and 59");
    }

    // Parse second argument
    for (int i = 0; i < strlen(argv[3]); i++)
    {
        if (!isdigit(argv[3][i]))
        {
            error("second argument must be numeric");
        }
    }
    second = atoi(argv[3]);
    if (second < 0 || second > 59)
    {
        error("second argument must be between 0 and 59");
    }

    // Parse path argument
    path = argv[4];
}

// sinyal SIGTERM. Jika sinyal tersebut diterima, maka program akan dihentikan
void handle_signal(int signal)
{
    if (signal == SIGTERM)
    {
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char **argv)
{
    parse_arguments(argc, argv);

    // Set up signal handler for SIGTERM
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = handle_signal;
    sigaction(SIGTERM, &action, NULL);

    // Daemonize the process
    pid_t pid = fork();
    if (pid < 0)
    {
        error("forking process failed");
    }
    else if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    // Set up time structure for current time
    time_t current_time;
    struct tm *time_info;

    while (true)
    {
        // Get current time
        time(&current_time);
        time_info = localtime(&current_time);

        // Check if current time matches cron configuration
        if ((hour == -1 || hour == time_info->tm_hour) && minute == time_info->tm_min && second == time_info->tm_sec)
        {
            // Execute script dengan execl
            pid_t script_pid = fork();
            if (script_pid < 0)
            {
                error("forking script process failed");
            }
            else if (script_pid == 0)
            {
                execl(path, path, NULL);
                error("executing script failed");
            }
        }

        // Sleep for one second
        sleep(1);
    }

    return 0;
}