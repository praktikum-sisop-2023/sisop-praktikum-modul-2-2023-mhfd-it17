#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>

#define FOLDER_PATH "./" // path lokasi folder

void create_folder(char *folder_name)
{
    pid_t pid;
    int status;

    // membuat child process
    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        // membuat folder dengan nama timestamp
        sprintf(cmd, "mkdir %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
    else
    {
        // menunggu child process selesai
        waitpid(pid, &status, 0);
    }
}

void download_image(char *folder_name, int image_number)
{
    char image_name[100];
    char image_path[100];
    time_t current_time = time(NULL);

    // menghitung nama gambar dengan format timestamp
    strftime(image_name, sizeof(image_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

    // menghitung ukuran gambar berdasarkan detik Epoch Unix
    int size = (current_time % 1000) + 50;

    // menghitung path lokasi gambar
    sprintf(image_path, "%s/%s/%s.jpg", FOLDER_PATH, folder_name, image_name);

    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        // download gambar dari https://picsum.photos/
        sprintf(cmd, "wget -O %s -q https://picsum.photos/%d", image_path, size);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
}

void zip_folder(char *folder_name)
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        // zip folder
        sprintf(cmd, "zip -r -q %s.zip %s", folder_name, folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        exit(0);
    }
    else
    {
        // wait for the zip process to finish
        waitpid(pid, &status, 0);

        // delete folder
        char cmd[100];
        sprintf(cmd, "rm -rf %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        exit(0);
    }
}

void create_killer_script(int mode, char *file, int p)
{
    FILE *script_killer = fopen("killer.sh", "w");
    // mode a (mematikan program dengan menghentikan semua proses)
    if (mode == 0)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "pkill -f -9 \"%s -a\"\n", file);
    }
    // mode b ( mematikan program dengan menghentikan proses dengan PID tertentu)
    else if (mode == 1)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "kill -9 \"%d\"\n", p);
    }
    // remove killer
    fprintf(script_killer, "rm -f killer.sh\n");
    fclose(script_killer);
    chmod("killer.sh", 0777); // add this line to give execute permission to the killer script
}

int main(int argc, char *argv[])
{
    char *m = argv[1];
    char *f = argv[0];

    pid_t pid, sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
     * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
     * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    int p = (int)getpid();

    if (argc >= 2 && strcmp(m, "-b"))
        create_killer_script(0, f, p);
    else
        // ketika argumen kedua bukan -b. fungsi killer akan dipanggil mode 1
        create_killer_script(1, f, p);

    while (1) // fungsi overlapping pada create folder
    {
        // membuat nama folder dengan format timestamp
        pid_t pid = fork();
        if (pid == 0)
        {
            char folder_name[100];
            time_t current_time = time(NULL);
            strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

            // membuat folder
            create_folder(folder_name);

            // download 15 gambar setiap 5 detik
            int i;
            for (i = 0; i < 15; i++)
            {
                download_image(folder_name, i);
                sleep(5);
            }

            // zip folder dan hapus folder
            zip_folder(folder_name);
        }

        // menunggu selama 30 detik sebelum membuat folder baru
        sleep(30);
    }

    return 0;
}