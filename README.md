# Sisop-Praktikum-Modul-2-2023-MHFD-IT17

Anggota Kelompok : 
1. Salmaa Satifha Dewi Rifma Putri  (5027211011)
2. Yoga Hartono                     (5027211023)
3. Dzakirozaan Uzlahwasata          (5027211066)

# Soal 1
## Analisa Soal
- Membuat folder sisopmodul2 sebagai tempat penyimpanan pekerjaan soal
- Mendownload file dari drive https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq
- Dilakukan unzip (tidak boleh menggunakan sistem saat meng-unzip)
- Melakukan pemilihan gambar secara acak dan ditampilkan ke layar
- Membuat folder HewanDarat, HewanAmphibi, dan HewanAir
- Memindahkan gambar hewan menyesuaikan dengan folder tempat tinggal
- Folder sebagai tempat gambar masing-masing habitat hewan diubah ke format zip (tidak boleh menggunakan sistem saat meng-zip)
- Untuk menghemat penyimpanan, maka folder HewanDarat, HewanAmphibi, dan HewanAir dihapus

## Cara Pengerjaan Soal 1
- Membuat folder sisopmodul2
```sh
mkdir sisopmodul2
```
- Membuat file binatang.c
```sh
nano binatang.c
```
- Membuat fungsi zip yang akan mengompresi direktori menjadi bentuk file.zip
```sh
int zip(char *filezip, char *direktori) {
    //mendefinisikan pointer z_info tipe data struct zip(simpan informasi file zip
    struct zip *z_info;
    //membuka file zip dengan nama filezip mode ZIP_CREATE
    if ((z_info = zip_open(filezip, ZIP_CREATE, NULL)) == NULL) 
    {
	//jika tidak dapat membuka file zip akan mengembalikan nilai 0 dan fungsi berhenti
        return 0;
    }

    //untuk membuka direktori yang akan di zip
    DIR *d_zip;
    //menyimpan informasi direktori yang akan di zip
    struct dirent *dir;
    //membuka direktori dengan path direktori dan disimpan di pointer d
    d_zip = opendir(direktori);
    //memeriksa apa direktori dapat dibuka(yg akan dizip)
    if (d_zip) 
    {
	    //jika iya, akan dilakukan iterasi setiap file dan folder
        while ((dir = readdir(d_zip)) != NULL) {
            //mendapat ekstensi file dari objek yang diiterasi = strrchr
            char *ext = strrchr(dir->d_name, '.');
            //dicek apa berupa jpg / jpeg = strcmp
            //jika iya akan ditambah file ke file zip
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                //buat path bagi file yang akan ditambah ke file zip = snprintf
                char path_file[256];
                snprintf(path_file, sizeof(path_file), "%s/%s", direktori, dir->d_name);

                //zip source = file yang akan ditambah ke file zip dgn fungsi zip source file
                struct zip_source *source = zip_source_file(z_info, path_file, 0, 0);
                //jika tidak bisa dibuat akan ngembaliin nilai 0
                if (source == NULL) 
                {
                    zip_close(z_info);
                    return 0;
                }
                //nambah file yg dibuat zip source ke zip dengan fungsi zip file add
                int err = zip_file_add(z_info, dir->d_name, source, ZIP_FL_OVERWRITE);
                //jika gabisa ditambah zip source dihapus+ditutup
                if (err < 0) 
                {
                    zip_source_free(source);
                    zip_close(z_info);
                    return 0;
                }
            }
        }
        closedir(d_zip);
    }
    //file zip ditutup sebagai tanda proses zip berhasil
    zip_close(z_info);
    return 1;
}
```
- fungsi bernama `zip` yang menerima dua parameter, yaitu `filezip` dan `direktori`. Fungsi ini untuk membuat file zip dari direktori yang diberikan dan menambahkan semua file dengan ekstensi `.jpg`, `.jpeg`, atau `.png` yang ada di dalam direktori tersebut ke dalam file zip yang dibuat.
- Pertama, fungsi ini mendefinisikan sebuah pointer `z_info` yang bertipe struct zip untuk menyimpan informasi file zip yang akan dibuat. Kemudian, fungsi membuka file zip dengan nama dan mode yang diberikan menggunakan fungsi `zip_open`. Jika file zip tidak dapat dibuka, fungsi akan mengembalikan nilai 0 dan berhenti.
- Selanjutnya, fungsi membuka direktori yang akan di-zip menggunakan fungsi `opendir`. Jika direktori dapat dibuka, maka akan dilakukan iterasi setiap file dan folder yang ada di dalam direktori menggunakan fungsi `readdir`. Setiap file akan dicek apakah memiliki ekstensi `.jpg`, `.jpeg`, atau `.png` menggunakan fungsi `strrchr` dan `strcmp`. Jika file tersebut memiliki ekstensi yang sesuai, maka fungsi akan membuat path untuk file tersebut menggunakan fungsi `snprintf`.
- Selanjutnya, fungsi membuat `zip source` untuk file tersebut menggunakan fungsi `zip_source_file`. Jika zip source tidak dapat dibuat, maka fungsi akan menutup file zip yang sudah dibuka dan mengembalikan nilai 0.
- Jika zip source berhasil dibuat, maka fungsi akan menambahkan file tersebut ke dalam file zip menggunakan fungsi `zip_file_add`. Jika file tidak berhasil ditambahkan, maka zip source akan dihapus menggunakan fungsi `zip_source_free`, file zip akan ditutup, dan fungsi akan mengembalikan nilai 0.
- Setelah iterasi selesai, fungsi akan menutup direktori yang telah dibuka menggunakan fungsi `closedir`. Terakhir, file zip akan ditutup menggunakan fungsi `zip_close` sebagai tanda bahwa proses zip telah berhasil dan fungsi akan mengembalikan nilai 1.


- Membuat fungsi utama untuk mendownload file
```sh
//a. download file
    system("wget -q \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\" -O binatang.zip");
    // Nama file zip yang hendak diunzip
    const char *filename = "binatang.zip";
```
- command `wget` digunakan untuk mendownload file `binatang.zip` dari Google Drive menggunakan link yang diberikan.

- Setelah file `binatang.zip` didownload, mengeset variabel `filename` sebagai nama file yang akan diunzip, yaitu `binatang.zip`. Variabel filename digunakan pada kode selanjutnya untuk membuka file zip dan mengakses isi file tersebut.


- Membuat fungsi untuk membuka file zip
```sh
//membuka file zip menggunakan fungsi zip_open
    struct zip *zip_file = zip_open(filename, 0, NULL);
    if (zip_file == NULL) {
        fprintf(stderr, "Error saat membuka file zip!!!\n");
        return 1;
    }
```
- mendeklarasi variabel `zip_file` sebagai pointer ke struktur zip, yang akan menampung file zip yang akan dibuka. Fungsi `zip_open` dipanggil untuk membuka file zip tersebut. Fungsi ini menerima tiga argumen: filename yang berisi nama file yang akan dibuka, 0 yang menunjukkan mode pembacaan (0 untuk hanya membaca, 1 untuk membaca dan menulis), dan NULL yang menunjukkan bahwa tidak ada opsi tambahan yang diberikan. Jika `zip_open` mengembalikan nilai NULL, maka akan dicetak pesan kesalahan di stderr (standar error) dan program akan mengembalikan nilai 1 untuk menunjukkan bahwa terjadi kesalahan saat membuka file zip.


- Membuat fungsi untuk melakukan unzip
```sh
//unzip setiap gambar yang ada di dalam zip
    int num_entries = zip_get_num_entries(zip_file, 0);
    for (int i = 0; i < num_entries; i++) {
        //struktur yang digunakan untuk menyimpan informasi tentang file di dalam file zip
        struct zip_stat file_stat;
        zip_stat_index(zip_file, i, 0, &file_stat);

        char fileOutput[256];
        strcpy(fileOutput, file_stat.name);

        //jika file yang akan diunzip berupa direktori, tidak akan dieksekusi
        if (fileOutput[strlen(fileOutput)-1] == '/') 
        {
            continue;
        }

        struct zip_file *file = zip_fopen_index(zip_file, i, 0);
        if (file == NULL) 
        {
            fprintf(stderr, "Error saat membuka file didalam zip!!!\n");
            zip_close(zip_file);
            return 1;
        }

        FILE *fp = fopen(fileOutput, "wb");
        if (fp == NULL) 
        {
            fprintf(stderr, "Error saat membuat output file!!!\n");
            zip_fclose(file);
            zip_close(zip_file);
            return 1;
        }

        char buffer[4096];
        int n;
        do 
        {
            n = zip_fread(file, buffer, sizeof(buffer));
            if (n > 0) {
                fwrite(buffer, n, 1, fp);
            }
        } while (n > 0);
        fclose(fp);
        zip_fclose(file);
    }
    //menutup file zip
    zip_close(zip_file);
    printf("a. File sudah diunzip\n");
```
- Pertama, variabel `num_entries` diinisialisasi dengan jumlah file dalam file zip.
- Loop for kemudian dimulai, dan berjalan sebanyak `num_entries` kali untuk mengakses setiap file dalam file zip.
- Untuk setiap file dalam file zip, informasi tentang file tersebut disimpan dalam struktur `file_stat` menggunakan `zip_stat_index()`.
- Nama file yang di-unzip disalin dari `file_stat.name` ke variabel `fileOutput`.
- Jika file yang akan diunzip adalah direktori (dalam hal ini ditandai dengan akhiran "/" pada nama file), maka loop akan melanjutkan ke file berikutnya dengan continue.
- Jika file bukan direktori, maka file akan dibuka menggunakan `zip_fopen_index()`. Jika file tidak dapat dibuka, pesan kesalahan akan dicetak dan program akan keluar dengan kode error 1.
- Setelah file berhasil dibuka, file output baru dibuat dengan nama yang sama dengan nama file yang di-unzip menggunakan `fopen()`. Jika file output tidak dapat dibuat, pesan kesalahan akan dicetak dan file input akan ditutup dan program akan keluar dengan kode error 1.
- Data dari file zip dibaca menggunakan `zip_fread()` dan disimpan dalam buffer buffer. Jumlah byte yang dibaca disimpan dalam variabel n.
- Jika n lebih besar dari 0, data akan ditulis ke file output menggunakan `fwrite()`.
- Proses membaca data dan menulis data dilakukan dalam loop do-while, hingga tidak ada lagi data yang harus dibaca dari file zip (n <= 0).
- Setelah semua data selesai dibaca dan ditulis ke file output, file output dan file input ditutup dengan `fclose()` dan `zip_fclose()`.
- Setelah semua file di-unzip, file zip akan ditutup dengan `zip_close()` dan pesan berhasil dicetak.


- Membuat fungsi untuk pemilihan gambar secara acak dengan pembuatan array untuk menyimpan file gambar yang valid
```sh
//b. pemilihan secara acak
    // membuat array of strings untuk menyimpan nama file yang valid
    const char *valid_extensions[] = {".jpg", ".jpeg", ".png"};
    int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);

    // membuka direktori tempat file di-unzip
    DIR *dir = opendir(".");
    if (dir == NULL) {
        fprintf(stderr, "Error saat membuka direktori\n");
        return 1;
    }

    // menyimpan semua nama file yang valid ke dalam array
    const int MAX_FILES = 1000;
    char filenames[MAX_FILES][256];
    int num_files = 0;
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        char *ext = strrchr(entry->d_name, '.');
        if (ext == NULL) {
            continue;
        }
        for (int i = 0; i < num_valid_extensions; i++) {
            if (strcmp(ext, valid_extensions[i]) == 0) {
                strcpy(filenames[num_files], entry->d_name);
                num_files++;
                break;
            }
        }
    }
    closedir(dir);
```
- `const char *valid_extensions[] = {".jpg", ".jpeg", ".png"};` : mendefinisikan sebuah array string konstan `const char *[]` dengan nama `valid_extensions`, yang berisi beberapa ekstensi file gambar yang valid, yaitu .jpg, .jpeg, dan .png.
- `int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);` : menghitung jumlah elemen dalam array `valid_extensions` dengan menggunakan `sizeof()`, dan membaginya dengan ukuran tipe data char*, sehingga menghasilkan jumlah elemen dalam array tersebut.
- `DIR *dir = opendir(".");` : membuka direktori saat ini `.`, dan menyimpan hasilnya ke dalam sebuah variabel bertipe `DIR*`.
- `if (dir == NULL) {...}` : mengecek apakah berhasil membuka direktori atau tidak. Jika gagal, maka akan mencetak pesan kesalahan ke konsol dan mengembalikan nilai 1.
- `const int MAX_FILES = 1000;` : mendefinisikan sebuah konstanta integer dengan nama `MAX_FILES`, yang akan digunakan sebagai ukuran maksimum array filenames.
- `char filenames[MAX_FILES][256];` : mendefinisikan sebuah array 2 dimensi filenames, yang akan digunakan untuk menyimpan nama-nama file gambar yang valid.
- `int num_files = 0;` : menginisialisasi variabel `num_files` dengan nilai 0, yang akan digunakan untuk menyimpan jumlah file gambar yang valid yang telah ditemukan.
- `struct dirent *entry;` : mendefinisikan sebuah pointer struct dirent dengan nama `entry`, yang akan digunakan untuk membaca setiap file dan direktori dalam direktori saat ini.
- `while ((entry = readdir(dir)) != NULL) {...}` : membaca setiap file dan direktori dalam direktori saat ini, dan menyimpan informasi tentang file/direktori tersebut ke dalam `entry`. Selama masih ada file/direktori yang belum dibaca, maka perintah di dalam kurung kurawal akan dijalankan.
- `char *ext = strrchr(entry->d_name, '.');` : mendefinisikan sebuah pointer karakter `ext`, yang diisi dengan alamat memori dari karakter titik `.` dalam nama file `entry->d_name`. Jika tidak ditemukan karakter titik, maka pointer `ext` akan bernilai NULL.
- `if (ext == NULL) {...}` : mengecek apakah ada karakter titik dalam nama file atau tidak. Jika tidak, maka program akan melanjutkan membaca file/direktori berikutnya dengan perintah continue.
- Loop for akan melakukan iterasi sebanyak `num_valid_extensions`, yaitu panjang dari array `valid_extensions`. Setiap iterasi, akan dicek apakah ekstensi file yang sedang diperiksa sama dengan salah satu ekstensi yang dinyatakan valid di array `valid_extensions`. Hal ini dilakukan dengan menggunakan fungsi `strcmp()` untuk membandingkan string `ext` yang menyimpan ekstensi file dengan string yang ada di `valid_extensions[i]`. Jika ekstensi file valid, maka nama file akan disalin ke dalam array `filenames` pada indeks `num_files` dengan menggunakan fungsi `strcpy()`. Setelah itu, nilai `num_files` akan ditambahkan 1 dan loop akan dihentikan dengan `break`.


- Me-random file dari array sebagai tempat penyimpanan file gambar dan dicetak
```sh
// mengacak nomor indeks file yang akan diambil dari array filenames
    srand(time(NULL));
    int random_index = rand() % num_files;
    char *random_filename = filenames[random_index];

    // mencetak file gambar yang terpilih secara acak
    printf("b. Gambar yang terpilih secara acak: %s\n", random_filename);
```
- `srand(time(NULL));` : mengatur seed dari fungsi `rand()` yang digunakan untuk menghasilkan angka acak yang diambil dari waktu sistem saat fungsi berjalan
- `int random_index = rand() % num_files;` : menghasilkan angka acak yang memiliki nilai antara 0 dan `num_files - 1` untuk memilih index file gambar yang terpilih
- `char *random_filename = filenames[random_index];` : mendefinisikan pointer `char *` dengan nama `random_filename`, diisi dengan alamat memori dari string yang merupakan nama file gambar terpilih secara acak. Nama file gambar ini diambil dari array filenames pada indeks yang telah dipilih secara acak pada baris sebelumnya.
- `printf("b. Gambar yang terpilih secara acak: %s\n", random_filename);` : mencetak pesan ke konsol, yang berisi nama file gambar terpilih secara acak.


- Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
```sh
//c. membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
    system("mkdir -p HewanDarat");
    system("mkdir -p HewanAmphibi");
    system("mkdir -p HewanAir");
```
- menjalankan perintah `mkdir` yaitu membuat direktori dengan nama `HewanDarat`, `HewanAir`, dan `HewanAmphibi` dengan opsi `-p` dimana jika direktori yang ditentukan tidak ada, maka perintah `mkdir` membuat direktori yang diperlukan sebelum membuat direktori utama


- Memindahkan file gambar ke dalam folder menyesuaikan habitat masing-masing
```sh
//memindahkan gambar ke direktori masing masing tempat tinggal
    system("mv *_darat.jpg HewanDarat");
    system("mv *_amphibi.jpg HewanAmphibi");
    system("mv *_air.jpg HewanAir");
    printf("c. Folder sudah dibuat dan File gambar sudah dipindah.\n");

    char dir1[] = "HewanDarat";
    char dir2[] = "HewanAmphibi";
    char dir3[] = "HewanAir";    
```
- `system("mv *_darat.jpg HewanDarat");`, `system("mv *_amphibi.jpg HewanAmphibi");`, dan `system("mv *_air.jpg HewanAir");` akan menjalankan perintah `mv` yang memindahkan semua file yang diakhiri dengan `_darat.jpg` ke direktori `HewanDarat`, `_air.jpg` ke direktori `HewanAir`, dan `_amphibi.jpg` ke direktori `HewanAmphibi`
- `printf("c. Folder sudah dibuat dan File gambar sudah dipindah.\n");` : akan mencetak string `c. Folder sudah dibuat dan File gambar sudah dipindah.` ke layar
- `char dir1[] = "HewanDarat";`, `char dir2[] = "HewanAmphibi";`, dan `char dir3[] = "HewanAir";` mendefinisikan 3 variabel karakter `dir` yang berisi string nama direktori `HewanDarat`, `HewanAir`, dan `HewanAmphibi`


- Melakukan zip dengan fungsi yang sudah dideklarasikan di awal pada folder hewan masing-masing habitat
```sh
//d. melakukan zip
    zip("HewanDarat.zip", dir1);
    zip("HewanAmphibi.zip", dir2);
    zip("HewanAir.zip", dir3);
```
- kode program diatas akan melakukan proses kompresi direktori menjadi file zip dengan format penamaan dari argumen pertama fungsi `zip()` yaitu `HewanDarat.zip`, `HewanAmphibi.zip`, dan `HewanAir.zip` yang berisi file dari direktori `dir1`, `dir2`, dan `dir3` sebagai argumen kedua yang akan menyesuaikan argumen pertamanya


- Menghapus folder masing-masing habitat hewan
```sh
    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    printf("d. Folder sudah zip dan folder dihapus.\n");
    return 0;
```
- `system("rm -r HewanDarat");` : dengan fungsi `system()` akan menjalankan perintah shell. `rm -r` akan menjalankan perintah hapus direktori HewanDarat beserta isi direktori tersebut, berlaku juga untuk direktori yang lain yaitu HewanAmphibi dan HewanAir
- `printf("d. Folder sudah zip dan folder dihapus.\n");` : akan mencetak string `d. Folder sudah zip dan folder dihapus.` ke layar


- Meng-compile pada terminal menggunakan `gcc binatang.c -o binatang -lzip` kemudian menjalankan `./binatang`




## Source Code
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

//fungsi zip yang akan mengompresi direktori menjadi file zip
int zip(char *filezip, char *direktori) {
    struct zip *z_info;
    if ((z_info = zip_open(filezip, ZIP_CREATE, NULL)) == NULL) 
    {
        return 0;
    }

    DIR *d_zip;
    struct dirent *dir;
    d_zip = opendir(direktori);
    if (d_zip) 
    {
        while ((dir = readdir(d_zip)) != NULL) {
            //mendapat ekstensi file dari objek yang diiterasi = strrchr
            char *ext = strrchr(dir->d_name, '.');
            //dicek apa berupa jpg / jpeg = strcmp
            //jika iya akan ditambah file ke file zip
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                //buat path bagi file yang akan ditambah ke file zip = snprintf
                char path_file[256];
                snprintf(path_file, sizeof(path_file), "%s/%s", direktori, dir->d_name);

                //zip source = file yang akan ditambah ke file zip dgn fungsi zip source file
                struct zip_source *source = zip_source_file(z_info, path_file, 0, 0);
                //jika tidak bisa dibuat akan ngembaliin nilai 0
                if (source == NULL) 
                {
                    zip_close(z_info);
                    return 0;
                }
                //nambah file yg dibuat zip source ke zip dengan fungsi zip file add
                int err = zip_file_add(z_info, dir->d_name, source, ZIP_FL_OVERWRITE);
                //jika gabisa ditambah zip source dihapus+ditutup
                if (err < 0) 
                {
                    zip_source_free(source);
                    zip_close(z_info);
                    return 0;
                }
            }
        }
        closedir(d_zip);
    }
    //file zip ditutup sebagai tanda proses zip berhasil
    zip_close(z_info);
    return 1;
}

int main() {
    //a. download file
    system("wget -q \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\" -O binatang.zip");
    // Nama file zip yang hendak diunzip
    const char *filename = "binatang.zip";

    //membuka file zip menggunakan fungsi zip_open
    struct zip *zip_file = zip_open(filename, 0, NULL);
    if (zip_file == NULL) {
        fprintf(stderr, "Error saat membuka file zip!!!\n");
        return 1;
    }

    //unzip setiap gambar yang ada di dalam zip
    int num_entries = zip_get_num_entries(zip_file, 0);
    for (int i = 0; i < num_entries; i++) {
        //struktur yang digunakan untuk menyimpan informasi tentang file di dalam file zip
        struct zip_stat file_stat;
        zip_stat_index(zip_file, i, 0, &file_stat);

        char fileOutput[256];
        strcpy(fileOutput, file_stat.name);

        //jika file yang akan diunzip berupa direktori, tidak akan dieksekusi
        if (fileOutput[strlen(fileOutput)-1] == '/') 
        {
            continue;
        }

        struct zip_file *file = zip_fopen_index(zip_file, i, 0);
        if (file == NULL) 
        {
            fprintf(stderr, "Error saat membuka file didalam zip!!!\n");
            zip_close(zip_file);
            return 1;
        }

        FILE *fp = fopen(fileOutput, "wb");
        if (fp == NULL) 
        {
            fprintf(stderr, "Error saat membuat output file!!!\n");
            zip_fclose(file);
            zip_close(zip_file);
            return 1;
        }

        char buffer[4096];
        int n;
        do 
        {
            n = zip_fread(file, buffer, sizeof(buffer));
            if (n > 0) {
                fwrite(buffer, n, 1, fp);
            }
        } while (n > 0);
        fclose(fp);
        zip_fclose(file);
    }
    //menutup file zip
    zip_close(zip_file);
    printf("a. File sudah diunzip\n");

    //b. pemilihan secara acak
    // membuat array of strings untuk menyimpan nama file yang valid
    const char *valid_extensions[] = {".jpg", ".jpeg", ".png"};
    int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);

    // membuka direktori tempat file di-unzip
    DIR *dir = opendir(".");
    if (dir == NULL) {
        fprintf(stderr, "Error saat membuka direktori\n");
        return 1;
    }

    // menyimpan semua nama file yang valid ke dalam array
    const int MAX_FILES = 1000;
    char filenames[MAX_FILES][256];
    int num_files = 0;
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        char *ext = strrchr(entry->d_name, '.');
        if (ext == NULL) {
            continue;
        }
        for (int i = 0; i < num_valid_extensions; i++) {
            if (strcmp(ext, valid_extensions[i]) == 0) {
                strcpy(filenames[num_files], entry->d_name);
                num_files++;
                break;
            }
        }
    }
    closedir(dir);

    // mengacak nomor indeks file yang akan diambil dari array filenames
    srand(time(NULL));
    int random_index = rand() % num_files;
    char *random_filename = filenames[random_index];

    // mencetak file gambar yang terpilih secara acak
    printf("b. Gambar yang terpilih secara acak: %s\n", random_filename);


    //c. membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
    system("mkdir -p HewanDarat");
    system("mkdir -p HewanAmphibi");
    system("mkdir -p HewanAir");

    //memindahkan gambar ke direktori masing masing tempat tinggal
    system("mv *_darat.jpg HewanDarat");
    system("mv *_amphibi.jpg HewanAmphibi");
    system("mv *_air.jpg HewanAir");
    printf("c. Folder sudah dibuat dan File gambar sudah dipindah.\n");

    char dir1[] = "HewanDarat";
    char dir2[] = "HewanAmphibi";
    char dir3[] = "HewanAir";

    //d. melakukan zip
    zip("HewanDarat.zip", dir1);
    zip("HewanAmphibi.zip", dir2);
    zip("HewanAir.zip", dir3);

    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    printf("d. Folder sudah zip dan folder dihapus.\n");
    return 0;
}
```

## Test Output
Mengecek Hasil Pemilihan Acak, Berhasil Melakukan Download, Zip, Unzip, Pemindahan Folder, dan Penghapusan Folder
![soal1](https://i.ibb.co/42RjXFn/Whats-App-Image-2023-04-08-at-07-58-51.jpg)

Mengecek Isi Folder HewanDarat
![soal1](https://i.ibb.co/9Y9XXrZ/a155b7c9-b749-42e8-974d-40aad691e8d3.jpg)

Mengecek Isi Folder HewanAir
![soal1](https://i.ibb.co/6NdSD4r/49cec99a-a0d9-4e5d-85e5-d1f4e4cdf2b5.jpg)

Mengecek Isi Folder HewanAmphibi
![soal1](https://i.ibb.co/KX6BjFQ/60f22c21-8f85-4df7-9553-1c3abcc377af.jpg)

# Soal 2
## Analisa Soal
- Membuat folder yang di dalamnya terdapat sebuah program c yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss]
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss]
- Setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete (sehingga hanya menyisakan .zip)
- Program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Membuat program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai (semua folder terisi gambar, terzip lalu di delete).
- Dengan catatan :
    - Tidak boleh menggunakan system()
    - Proses berjalan secara daemon
    - Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

## Cara Pengerjaan Soal 2
- Define path lokasi folder
```sh
#define FOLDER_PATH "./"
```
- Membuat fungsi create_folder untuk membuat folder
```sh
void create_folder(char *folder_name)
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        sprintf(cmd, "mkdir %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
    else
    {
        waitpid(pid, &status, 0);
    }
}
```
- Membuat child process `pid = fork();`.   
- Membuat fungsi untuk membuat folder dengan nama timestamp `sprintf(cmd, "mkdir %s", folder_name);` `execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);` karena tidak boleh memakai system().   
- Menunggu child process selesai `waitpid(pid, &status, 0);`

- Membuat fungsi download_image untuk mendownload gambar sesuai ketentuan yang ada
```sh
void download_image(char *folder_name, int image_number)
{
    char image_name[100];
    char image_path[100];
    time_t current_time = time(NULL);

    strftime(image_name, sizeof(image_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

    int size = (current_time % 1000) + 50;

    sprintf(image_path, "%s/%s/%s.jpg", FOLDER_PATH, folder_name, image_name);

    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        sprintf(cmd, "wget -O %s -q https://picsum.photos/%d", image_path, size);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
}
```
- Menghitung nama gambar dengan format timestamp `strftime(image_name, sizeof(image_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));`
- Menghitung ukuran gambar berdasarkan detik Epoch Unix `int size = (current_time % 1000) + 50;`
- Menghitung path lokasi gambar `sprintf(image_path, "%s/%s/%s.jpg", FOLDER_PATH, folder_name, image_name);`
- Download gambar dari https://picsum.photos/ `sprintf(cmd, "wget -O %s -q https://picsum.photos/%d", image_path, size);` `execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);`

- Membuat fungsi zip_folder untuk melakukan zip folder sesuai ketentuan yang ada
```sh
void zip_folder(char *folder_name)
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        // zip folder
        sprintf(cmd, "zip -r -q %s.zip %s", folder_name, folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        exit(0);
    }
    else
    {
        // wait for the zip process to finish
        waitpid(pid, &status, 0);

        // delete folder
        char cmd[100];
        sprintf(cmd, "rm -rf %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd2, NULL);
        exit(0);

    }
}
```
- `sprintf(cmd, "zip -r -q %s.zip %s", folder_name, folder_name);` `execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);` untuk melakukan zip folder
- `waitpid(pid, &status, 0);` menunggu proses zip selesai
- `sprintf(cmd, "rm -rf %s", folder_name);` `execl("/bin/sh", "/bin/sh", "-c", cmd2, NULL);` untuk melakukan delete folder

- Membuat fungsi create_killer_script untuk membuat script killer yang memiliki 2 mode dan sesuai ketentuan
```sh
void create_killer_script(int mode, char *file, int p)
{
    FILE *script_killer = fopen("killer.sh", "w");
    if (mode == 0)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "pkill -f -9 \"%s -b\"\n", file);
    }
    else if (mode == 1)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "kill -9 \"%d\"\n", p);
    }
    fprintf(script_killer, "rm -f killer.sh\n");
    fclose(script_killer);
    chmod("killer.sh", 0777);
}
```
- `fprintf(script_killer, "pkill -f -9 \"%s -b\"\n", file);` mode a (mematikan program dengan menghentikan semua proses)
- `fprintf(script_killer, "kill -9 \"%d\"\n", p);` mode b ( mematikan program dengan menghentikan proses dengan PID tertentu)
- `fprintf(script_killer, "rm -f killer.sh\n");` untuk melakukan remove killer
- `chmod("killer.sh", 0777);` untuk memberikan permission saat mengeksekusi script killer

- Fungsi main untuk menjalankan fungsi yang ada sesuai ketentuan
```sh
int main(int argc, char *argv[])
{
    char *m = argv[1];
    char *f = argv[0];

    pid_t pid, sid;

    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    int p = (int)getpid();

    if (argc >= 2 && strcmp(m, "-b"))
        create_killer_script(0, f, p);
    else
        create_killer_script(1, f, p);

    while (1)
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            char folder_name[100];
            time_t current_time = time(NULL);
            strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

            create_folder(folder_name);

            int i;
            for (i = 0; i < 15; i++)
            {
                download_image(folder_name, i);
                sleep(5);
            }

            zip_folder(folder_name);
        }

        sleep(30);
    }

    return 0;
}
```
- `pid_t pid, sid;` Variabel untuk menyimpan PID
- `pid = fork();` Menyimpan PID dari Child Process
- `if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }` Keluar saat fork gagal (nilai variabel pid < 0)
- `if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }` Keluar saat fork berhasil (nilai variabel pid adalah PID dari child process)
- `create_killer_script(1, f, p);` ketika argumen kedua bukan -b. fungsi killer akan dipanggil mode 1
- `while (1)` fungsi overlapping pada create folder
- `strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));` membuat nama folder dengan format timestamp
- `create_folder(folder_name);` membuat folder
- `int i;
            for (i = 0; i < 15; i++)
            {
                download_image(folder_name, i);
                sleep(5);
            }` download 15 gambar setiap 5 detik
- `zip_folder(folder_name);` zip folder dan hapus folder
- `sleep(30);` menunggu selama 30 detik sebelum membuat folder baru

## Source Code
```sh
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>

#define FOLDER_PATH "./"

void create_folder(char *folder_name)
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        sprintf(cmd, "mkdir %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
    else
    {
        waitpid(pid, &status, 0);
    }
}

void download_image(char *folder_name, int image_number)
{
    char image_name[100];
    char image_path[100];
    time_t current_time = time(NULL);

    strftime(image_name, sizeof(image_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

    int size = (current_time % 1000) + 50;

    sprintf(image_path, "%s/%s/%s.jpg", FOLDER_PATH, folder_name, image_name);

    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        sprintf(cmd, "wget -O %s -q https://picsum.photos/%d", image_path, size);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);

        exit(0);
    }
}

void zip_folder(char *folder_name)
{
    pid_t pid;
    int status;

    pid = fork();

    if (pid < 0)
    {
        fprintf(stderr, "fork failed\n");
        exit(1);
    }
    else if (pid == 0)
    {
        char cmd[100];

        sprintf(cmd, "zip -r -q %s.zip %s", folder_name, folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
        exit(0);
    }
    else
    {
        waitpid(pid, &status, 0);

        char cmd[100];
        sprintf(cmd, "rm -rf %s", folder_name);
        execl("/bin/sh", "/bin/sh", "-c", cmd2, NULL);
        exit(0);

    }
}

void create_killer_script(int mode, char *file, int p)
{
    FILE *script_killer = fopen("killer.sh", "w");
    if (mode == 0)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "pkill -f -9 \"%s -b\"\n", file);
    }
    else if (mode == 1)
    {
        fprintf(script_killer, "#!/bin/bash\n");
        fprintf(script_killer, "kill -9 \"%d\"\n", p);
    }
    fprintf(script_killer, "rm -f killer.sh\n");
    fclose(script_killer);
    chmod("killer.sh", 0777);
}

int main(int argc, char *argv[])
{
    char *m = argv[1];
    char *f = argv[0];

    pid_t pid, sid;

    pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    int p = (int)getpid();

    if (argc >= 2 && strcmp(m, "-b"))
        create_killer_script(0, f, p);
    else
        create_killer_script(1, f, p);

    while (1) 
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            char folder_name[100];
            time_t current_time = time(NULL);
            strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", localtime(&current_time));

            create_folder(folder_name);

            int i;
            for (i = 0; i < 15; i++)
            {
                download_image(folder_name, i);
                sleep(5);
            }

            zip_folder(folder_name);
        }

        sleep(30);
    }

    return 0;
}
```
## Test Ouput
- Ouput mode lukisan -a    
![soal2](https://i.ibb.co/42hr1FD/lukisan-a.png)

- Output mode lukisan -b   
![soal2](https://i.ibb.co/sPVCJ4y/lukisan-b.png)

- Output ukuran gambar   
![soal2](https://i.ibb.co/v3jZvvw/unix-time.png)

# Soal 3
## Analisa Soal
Ten Hag mendapat sebuah link gdrive dari sebuah file zip yang berisi database pemain-pemain bola. Diperlukankan sebuah program yang dapat melakukan beberapa task diantaranya:

Mengunduh file zip dari link gdrive, extract, lalu hapus file zip nya.
Menghapus pemain yang bukan dari Manchester United.
Mengkategorikan pemain berdasaran posisi nya, yaitu "Kiper", Bek", "Gelandang", dan "Penyerang".
Mencari kesebelasan terbaik, di output kan di dalam file .txt

Dengan syaratnya antara lain:

Tidak boleh menggunakan system()
Tidak boleh memakai function C mkdir() ataupun rename().
Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

## Cara Pengerjaan Soal 3
- Buat `filter.c`
- Library
```sh 
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>
```
- Membuat  fungsi bernama `downloadFile()` yang mengunduh sebuah file dari URL yang ditentukan menggunakan perintah wget di terminal. Fungsi ini akan mengembalikan nilai `true` jika unduhan berhasil dan `false` jika gagal.
```sh 
bool downloadFile() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("wget", "wget", "-q", "-O", ZIP_FILE_NAME, URL, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }

  return false;
}
```
- Membuat fungsi bernama `extractZip()` yang digunakan untuk mengekstrak file zip yang telah diunduh sebelumnya menggunakan perintah unzip di terminal. Fungsi ini mengembalikan nilai `true` jika proses ekstraksi berhasil dan `false` jika gagal.
```sh 
bool extractZip() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("unzip", "unzip", "-q", ZIP_FILE_NAME, "-d", EXTRACTED_FOLDER_NAME, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }
  return false;
}
```
- Membuat fungsi bernama `buatFolder()` dan digunakan untuk membuat empat folder yang masing-masing berisi daftar pemain sepak bola berdasarkan posisinya. Empat folder tersebut adalah "./players/players/Kiper", "./players/players/Bek", "./players/players/Gelandang", dan "./players/players/Penyerang".
```sh 
void buatFolder() {
  char *folderName[] = {"./players/players/Kiper", "./players/players/Bek","./players/players/Gelandang","./players/players/Penyerang"};
  pid_t pid = fork();
  if (pid == 0) {
    for (int i = 0; i < 4; i++) {
      pid_t pid = fork();
      char *args[] = {"mkdir", "-p", folderName[i], NULL};
      if (pid == 0) {
        execvp("mkdir", args);
      } else if (pid > 0) {
        waitpid(pid, NULL, 0);
      }
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}
```
- Membuat fungsi bernama `hapusNonManUtdPlayers()` dan digunakan untuk menghapus daftar pemain yang bukan berasal dari tim sepak bola Manchester United. Fungsi dimulai dengan membuka direktori "./players/players" menggunakan pemanggilan opendir() dan menyimpan hasilnya dalam variabel dir.
```sh 
void hapusNonManUtdPlayers() {
  DIR *dir;
  struct dirent *entry;
  char filename[256], team[256], *p, path[512];
  dir = opendir("./players/players");
  pid_t pid;
  pid = fork();
  if (pid == 0) {
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            if (strcmp(p, "ManUtd") != 0) {
              sprintf(path, "./players/players/%s", entry->d_name);
              remove(path);
            }
          }
        }
      }
      closedir(dir);
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}
```
- Membuat fungsi `kategoriPlayers`, fungsi ini merupakan sebuah program untuk mengkategorikan pemain dalam sebuah tim sepak bola ke dalam empat kategori berdasarkan posisi mereka di lapangan, yaitu Kiper, Bek, Gelandang, dan Penyerang.
```sh 
void kategoriPlayers() {
  pid_t pid, create_dir;
  struct dirent *entry;

  char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"}, filename[256],
       *p, path[512];
  DIR *dir;

  for (int i = 0; i < 4; i++) {

    pid_t pid;
    pid = fork();
    dir = opendir("./players/players");

    if (pid == 0 && dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            sprintf(path, "./players/players/%s", entry->d_name);
            char folder_path[50];
            if (strcmp(positions[i], p) == 0) {
              sprintf(folder_path, "./players/players/%s", positions[i]);
              char *args[] = {"mv", path, folder_path, NULL};
              pid_t cid;
              cid = fork();
              if (cid == 0)
                execvp("mv", args);
              else if (cid > 0) {
                waitpid(cid, NULL, 0);
              }
            }
          }
        }
      }
    } else if (pid > 0) {
      waitpid(pid, NULL, 0);
      exit(0);
    }
  }
}
```
- Fungsi digunakan sebagai parameter untuk memanggil fungsi `qsort()`, yaitu fungsi untuk melakukan pengurutan data dalam sebuah array. Fungsi ini akan membandingkan dua elemen dalam array dan akan mengembalikan nilai yang menunjukkan posisi elemen tersebut dalam array.
```sh 
int compare(const void *a, const void *b) {
    const int *int_a = (const int*)a;
    const int *int_b = (const int*)b;
    
    if (*int_a > *int_b) {
        return -1;
    } else if (*int_a == *int_b) {
        return 0;
    } else {
        return 1;
    }
}
```
- Membuat fungsi  sebuah fungsi bernama `buatTim` yang digunakan untuk membuat sebuah file berisi formasi dari sebuah tim sepak bola berdasarkan jumlah pemain di setiap posisi. Fungsi ini memiliki tiga parameter masukan yaitu `b`, `g`, dan `p` yang masing-masing merepresentasikan jumlah pemain di posisi bek, gelandang, dan penyerang.
```sh 
void buatTim(int b, int g, int p) {
  int pemain[3] = {b, g, p};
  char *positions[] = {"./players/players/Bek", "./players/players/Gelandang",
                       "./players/players/Penyerang"},
       filename[256];
  FILE *fp;
  char nama_file[100];
  char *home_dir = getenv("HOME");
  sprintf(nama_file, "%s/Formasi_%d-%d-%d.txt", home_dir, b, g, p);
  fp = fopen(nama_file, "w");
  fputs("./players/Kiper\nDeGea\n", fp);
  for (int i = 0; i < 3; i++) {
    char p[100];
    sprintf(p, "%s\n", positions[i]);
    fputs(p, fp);
    DIR *dir;
    int arr[10];
    struct dirent *entry;
    dir = opendir(positions[i]);
    int j = 0;
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          arr[j] = atoi(p);
          j++;
        }
      }
      closedir(dir);
      qsort(arr, j, sizeof(int), compare);
      char strings[20][10];
      for (int l = 0; l < pemain[i]; l++) {
        dir = opendir(positions[i]);
        while ((entry = readdir(dir)) != NULL) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          if (p != NULL && arr[l] == atoi(p) &&
              strcmp(filename, strings[l - 1]) != 0) {
            strcpy(strings[l], filename);
            strcat(filename, "\n");
            fputs(filename, fp);
            break;
          }
        }
      }
    }
  }
}
```
- Fungsi `main()` adalah fungsi utama dalam program C yang akan dijalankan pertama kali ketika program dijalankan. Fungsi ini memiliki tiga parameter, yaitu `argc` yang menunjukkan jumlah argumen yang diberikan ke program, argv yang merupakan array dari string yang berisi argumen-argumen tersebut, dan envp yang merupakan array dari string yang berisi variabel lingkungan.

Pada kode tersebut, fungsi `main()` terdiri dari beberapa bagian yang meliputi:
- Mengunduh file zip dengan fungsi `downloadFile()`
- Mengekstrak file zip dengan fungsi `extractZip()`
- Menghapus file zip dengan `remove(ZIP_FILE_NAME)`
- Membuat folder positions dengan `buatFolder()`
- Menghapus pemain non-Manchester United dari dataset dengan `hapusNonManUtdPlayers()`
- Mengkategorikan pemain berdasarkan posisi dengan `kategoriPlayers()`
- Membuat tim dengan memanggil fungsi `buatTim()`, yang meminta input dari pengguna untuk jumlah pemain di setiap posisi dan mencetak formasi tim ke file yang ditentukan.

Terakhir, program menyelesaikan eksekusinya dengan mengembalikan nilai 0 dari fungsi `main().`
```sh 
int main(int argc, char *argv[]) { 

int b;
int g;
int p;

  printf("Downloading zip file...\n");

  if (!downloadFile()) {
    printf("Failed to download zip file!\n");
    return 1;
  }

  printf("Extracting zip file...\n");

  if (!extractZip()) {
    printf("Failed to extract zip file!\n");
    return 1;
  }

  printf("Deleting zip file...\n");

  if (remove(ZIP_FILE_NAME) == 0) {
    printf("Zip file deleted successfully!\n");
  } else {
    printf("Failed to delete zip file!\n");
    return 1;
  }
  printf("Creating positions folder...\n");
  buatFolder();
  printf("Remove Non MU players...\n");
  hapusNonManUtdPlayers();
  printf("Categorize players position...\n");
  kategoriPlayers();
  printf("Make team...\n");
  pid_t pid_tim;
  pid_tim = fork();

  if (pid_tim == 0) {
    printf("Masukkan Jumlah Bek: ");
    scanf("%d", &b);
    printf("Masukkan Jumlah Gelandang: ");
    scanf("%d", &g);
    printf("Masukkan Jumlah Penyerang: ");
    scanf("%d", &p);
    buatTim(b, g, p);

  } else if (pid_tim > 0) {
    waitpid(pid_tim, NULL, 0);
    exit(0);
  }

  return 0;
}
```
- Untuk meng-compile sekaligus meng-run code nya :
```sh
gcc -o filter filter.c ./filter
```
## Source Code
```sh 
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>

#define ZIP_FILE_NAME "players.zip"
#define EXTRACTED_FOLDER_NAME "players"
#define KIPER "kiper"
#define BEK "bek"
#define GELANDANG "gelandang"
#define PENYERANG "penyerang"
#define URL "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"  

bool downloadFile() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("wget", "wget", "-q", "-O", ZIP_FILE_NAME, URL, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }

  return false;
}

bool extractZip() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("unzip", "unzip", "-q", ZIP_FILE_NAME, "-d", EXTRACTED_FOLDER_NAME, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }
  return false;
}

void buatFolder() {
  char *folderName[] = {"./players/players/Kiper", "./players/players/Bek","./players/players/Gelandang","./players/players/Penyerang"};
  pid_t pid = fork();
  if (pid == 0) {
    for (int i = 0; i < 4; i++) {
      pid_t pid = fork();
      char *args[] = {"mkdir", "-p", folderName[i], NULL};
      if (pid == 0) {
        execvp("mkdir", args);
      } else if (pid > 0) {
        waitpid(pid, NULL, 0);
      }
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}

void hapusNonManUtdPlayers() {
  DIR *dir;
  struct dirent *entry;
  char filename[256], team[256], *p, path[512];
  dir = opendir("./players/players");
  pid_t pid;
  pid = fork();
  if (pid == 0) {
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            if (strcmp(p, "ManUtd") != 0) {
              sprintf(path, "./players/players/%s", entry->d_name);
              remove(path);
            }
          }
        }
      }
      closedir(dir);
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}

void kategoriPlayers() {
  pid_t pid, create_dir;
  struct dirent *entry;

  char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"}, filename[256],
       *p, path[512];
  DIR *dir;

  for (int i = 0; i < 4; i++) {

    pid_t pid;
    pid = fork();
    dir = opendir("./players/players");

    if (pid == 0 && dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            sprintf(path, "./players/players/%s", entry->d_name);
            char folder_path[50];
            if (strcmp(positions[i], p) == 0) {
              sprintf(folder_path, "./players/players/%s", positions[i]);
              char *args[] = {"mv", path, folder_path, NULL};
              pid_t cid;
              cid = fork();
              if (cid == 0)
                execvp("mv", args);
              else if (cid > 0) {
                waitpid(cid, NULL, 0);
              }
            }
          }
        }
      }
    } else if (pid > 0) {
      waitpid(pid, NULL, 0);
      exit(0);
    }
  }
}

int compare(const void *a, const void *b) {
    const int *int_a = (const int*)a;
    const int *int_b = (const int*)b;
    
    if (*int_a > *int_b) {
        return -1;
    } else if (*int_a == *int_b) {
        return 0;
    } else {
        return 1;
    }
}

void buatTim(int b, int g, int p) {
  int pemain[3] = {b, g, p};
  char *positions[] = {"./players/players/Bek", "./players/players/Gelandang",
                       "./players/players/Penyerang"},
       filename[256];
  FILE *fp;
  char nama_file[100];
  char *home_dir = getenv("HOME");
  sprintf(nama_file, "%s/Formasi_%d-%d-%d.txt", home_dir, b, g, p);
  fp = fopen(nama_file, "w");
  fputs("./players/Kiper\nDeGea\n", fp);
  for (int i = 0; i < 3; i++) {
    char p[100];
    sprintf(p, "%s\n", positions[i]);
    fputs(p, fp);
    DIR *dir;
    int arr[10];
    struct dirent *entry;
    dir = opendir(positions[i]);
    int j = 0;
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          arr[j] = atoi(p);
          j++;
        }
      }
      closedir(dir);
      qsort(arr, j, sizeof(int), compare);
      char strings[20][10];
      for (int l = 0; l < pemain[i]; l++) {
        dir = opendir(positions[i]);
        while ((entry = readdir(dir)) != NULL) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          if (p != NULL && arr[l] == atoi(p) &&
              strcmp(filename, strings[l - 1]) != 0) {
            strcpy(strings[l], filename);
            strcat(filename, "\n");
            fputs(filename, fp);
            break;
          }
        }
      }
    }
  }
}

int main(int argc, char *argv[]) { 

int b;
int g;
int p;

  printf("Downloading zip file...\n");

  if (!downloadFile()) {
    printf("Failed to download zip file!\n");
    return 1;
  }

  printf("Extracting zip file...\n");

  if (!extractZip()) {
    printf("Failed to extract zip file!\n");
    return 1;
  }

  printf("Deleting zip file...\n");

  if (remove(ZIP_FILE_NAME) == 0) {
    printf("Zip file deleted successfully!\n");
  } else {
    printf("Failed to delete zip file!\n");
    return 1;
  }
  printf("Creating positions folder...\n");
  buatFolder();
  printf("Remove Non MU players...\n");
  hapusNonManUtdPlayers();
  printf("Categorize players position...\n");
  kategoriPlayers();
  printf("Make team...\n");
  pid_t pid_tim;
  pid_tim = fork();

  if (pid_tim == 0) {
    printf("Masukkan Jumlah Bek: ");
    scanf("%d", &b);
    printf("Masukkan Jumlah Gelandang: ");
    scanf("%d", &g);
    printf("Masukkan Jumlah Penyerang: ");
    scanf("%d", &p);
    buatTim(b, g, p);

  } else if (pid_tim > 0) {
    waitpid(pid_tim, NULL, 0);
    exit(0);
  }

  return 0;
}
```
## Test Output
![image](/uploads/0c4d06685e74cec5f0db09ff81501482/image.png)
![image](/uploads/3c289e114f66495622a6d423e73b6528/image.png)
![image](/uploads/81d249bbf3417f113e87242de6a443b0/image.png)
![image](/uploads/8c2b7cb598285f7f820711e1371d090f/image.png)
![image](/uploads/47f7bcb9dccea8677b89235e96073134/image.png)
![image](/uploads/dd5823f016339f6850188ca2e76c1bbc/image.png)
![image](/uploads/4406f00c05c9f34939b5ae88f8ae7537/image.png)
![image](/uploads/e43771d5f9daa5ef1c231a0e9f4de5a9/image.png)

# Soal 4
## Analisa Soal
- Tidak menggunakan fungsi system()
- Membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Program dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error bebas
- Program berjalan dalam background dan hanya menerima satu config cron
- CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

## Cara Pengerjaan Soal 4
- Membuat fungsi error untuk melakukan error handling ketika argumen yang diterima program tidak sesuai
```sh
void error(char *message)
{
    printf("error: %s\n", message);
    exit(EXIT_FAILURE);
}
```

- Membuat fungsi parse_argument untuk memparse argumen yang diberikan pada command line. Fungsi ini menerima dua parameter
```sh
void parse_arguments(int argc, char **argv)
{
    if (argc != 5)
    {
        error("incorrect number of arguments");
    }

    if (strcmp(argv[1], "*") == 0)
    {
        hour = -1;
    }
    else
    {
        for (int i = 0; i < strlen(argv[1]); i++)
        {
            if (!isdigit(argv[1][i]))
            {
                error("hour argument must be numeric");
            }
        }

        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23)
        {
            error("hour argument must be between 0 and 23");
        }
    }

    for (int i = 0; i < strlen(argv[2]); i++)
    {
        if (!isdigit(argv[2][i]))
        {
            error("minute argument must be numeric");
        }
    }
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59)
    {
        error("minute argument must be between 0 and 59");
    }

    for (int i = 0; i < strlen(argv[3]); i++)
    {
        if (!isdigit(argv[3][i]))
        {
            error("second argument must be numeric");
        }
    }
    second = atoi(argv[3]);
    if (second < 0 || second > 59)
    {
        error("second argument must be between 0 and 59");
    }

    path = argv[4];
}
```
- `if (argc != 5)` jumlah argumen
- `if (strcmp(argv[1], "*") == 0)``hour = atoi(argv[1]);if (hour < 0 || hour > 23)` Parse hour argument
- `for (int i = 0; i < strlen(argv[1]); i++)
        {
            if (!isdigit(argv[1][i]))
            {
                error("hour argument must be numeric");
            }
        }` cek apakah input hanya terdiri dari angka
- `hour = atoi(argv[1]);` dikonversi bilangan bulat atoi
- `for (int i = 0; i < strlen(argv[2]); i++)``minute = atoi(argv[2]);
    if (minute < 0 || minute > 59)` Parse minute argument dan second argument
- `path = argv[4];` Parse path argument

- Membuat fungsi handle_signal untuk membuat sinyal SIGTERM. Jika sinyal tersebut diterima, maka program akan dihentikan
```sh
void handle_signal(int signal)
{
    if (signal == SIGTERM)
    {
        exit(EXIT_SUCCESS);
    }
}
```

- Fungsi main untuk menjalankan fungsi yang ada sesuai ketentuan 
```sh
int main(int argc, char **argv)
{
    parse_arguments(argc, argv);

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = handle_signal;
    sigaction(SIGTERM, &action, NULL);

    pid_t pid = fork();
    if (pid < 0)
    {
        error("forking process failed");
    }
    else if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    time_t current_time;
    struct tm *time_info;

    while (true)
    {
        time(&current_time);
        time_info = localtime(&current_time);

        if ((hour == -1 || hour == time_info->tm_hour) && minute == time_info->tm_min && second == time_info->tm_sec)
        {
            pid_t script_pid = fork();
            if (script_pid < 0)
            {
                error("forking script process failed");
            }
            else if (script_pid == 0)
            {
                execl(path, path, NULL);
                error("executing script failed");
            }
        }

        sleep(1);
    }

    return 0;
}
```
- `struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = handle_signal;
    sigaction(SIGTERM, &action, NULL);` Set up signal handler untuk SIGTERM
- `pid_t pid = fork();` membuat proses daemon atau berjalan pada background
- `time_t current_time;``struct tm *time_info;` Set up struktur waktu dengan current time
- `time(&current_time);``time_info = localtime(&current_time);` Mendapatkan current time
- `if ((hour == -1 || hour == time_info->tm_hour) && minute == time_info->tm_min && second == time_info->tm_sec)` Cek jika current time cocok dengan cron configuration
- `execl(path, path, NULL);` Execute script dengan execl
- `sleep(1);` Sleep selama satu detik agar CPU State berjalan secara minimum

## Source Code
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>

#define MAX_LENGTH 10  24

char *path;
int hour, minute, second;

void error(char *message)
{
    printf("error: %s\n", message);
    exit(EXIT_FAILURE);
}

void parse_arguments(int argc, char **argv)
{
    if (argc != 5)
    {
        error("incorrect number of arguments");
    }

    if (strcmp(argv[1], "*") == 0)
    {
        hour = -1;
    }
    else
    {
        for (int i = 0; i < strlen(argv[1]); i++)
        {
            if (!isdigit(argv[1][i]))
            {
                error("hour argument must be numeric");
            }
        }

        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23)
        {
            error("hour argument must be between 0 and 23");
        }
    }

    for (int i = 0; i < strlen(argv[2]); i++)
    {
        if (!isdigit(argv[2][i]))
        {
            error("minute argument must be numeric");
        }
    }
    minute = atoi(argv[2]);
    if (minute < 0 || minute > 59)
    {
        error("minute argument must be between 0 and 59");
    }

    for (int i = 0; i < strlen(argv[3]); i++)
    {
        if (!isdigit(argv[3][i]))
        {
            error("second argument must be numeric");
        }
    }
    second = atoi(argv[3]);
    if (second < 0 || second > 59)
    {
        error("second argument must be between 0 and 59");
    }

    path = argv[4];
}

void handle_signal(int signal)
{
    if (signal == SIGTERM)
    {
        exit(EXIT_SUCCESS);
    }
}

int main(int argc, char **argv)
{
    parse_arguments(argc, argv);

    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = handle_signal;
    sigaction(SIGTERM, &action, NULL);

    pid_t pid = fork();
    if (pid < 0)
    {
        error("forking process failed");
    }
    else if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    time_t current_time;
    struct tm *time_info;

    while (true)
    {
        time(&current_time);
        time_info = localtime(&current_time);

        if ((hour == -1 || hour == time_info->tm_hour) && minute == time_info->tm_min && second == time_info->tm_sec)
        {
            pid_t script_pid = fork();
            if (script_pid < 0)
            {
                error("forking script process failed");
            }
            else if (script_pid == 0)
            {
                execl(path, path, NULL);
                error("executing script failed");
            }
        }

        sleep(1);
    }

    return 0;
}
```

## Test Output
- Output daemon proses, eksekusi skrip bash, dan cpu state
![soal4](https://i.ibb.co/jr6Wq2s/soal-4.png)
