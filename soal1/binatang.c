#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

//fungsi zip yang akan mengompresi direktori menjadi file zip
//mendefinisikan fungsi bernama zip yang menerima 2 parameter -> nama file zip + direktori yang akan di zip
int zip(char *filezip, char *direktori) {
    //mendefinisikan pointer z_info tipe data struct zip(simpan informasi file zip
    struct zip *z_info;
    //membuka file zip dengan nama filezip mode ZIP_CREATE
    if ((z_info = zip_open(filezip, ZIP_CREATE, NULL)) == NULL) 
    {
	//jika tidak dapat membuka file zip akan mengembalikan nilai 0 dan fungsi berhenti
        return 0;
    }

    //untuk membuka direktori yang akan di zip
    DIR *d_zip;
    //menyimpan informasi direktori yang akan di zip
    struct dirent *dir;
    //membuka direktori dengan path direktori dan disimpan di pointer d
    d_zip = opendir(direktori);
    //memeriksa apa direktori dapat dibuka(yg akan dizip)
    if (d_zip) 
    {
	    //jika iya, akan dilakukan iterasi setiap file dan folder
        while ((dir = readdir(d_zip)) != NULL) {
            //mendapat ekstensi file dari objek yang diiterasi = strrchr
            char *ext = strrchr(dir->d_name, '.');
            //dicek apa berupa jpg / jpeg = strcmp
            //jika iya akan ditambah file ke file zip
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0)) {
                //buat path bagi file yang akan ditambah ke file zip = snprintf
                char path_file[256];
                snprintf(path_file, sizeof(path_file), "%s/%s", direktori, dir->d_name);

                //zip source = file yang akan ditambah ke file zip dgn fungsi zip source file
                struct zip_source *source = zip_source_file(z_info, path_file, 0, 0);
                //jika tidak bisa dibuat akan ngembaliin nilai 0
                if (source == NULL) 
                {
                    zip_close(z_info);
                    return 0;
                }
                //nambah file yg dibuat zip source ke zip dengan fungsi zip file add
                int err = zip_file_add(z_info, dir->d_name, source, ZIP_FL_OVERWRITE);
                //jika gabisa ditambah zip source dihapus+ditutup
                if (err < 0) 
                {
                    zip_source_free(source);
                    zip_close(z_info);
                    return 0;
                }
            }
        }
        closedir(d_zip);
    }
    //file zip ditutup sebagai tanda proses zip berhasil
    zip_close(z_info);
    return 1;
}

int main() {
    //a. download file
    system("wget -q \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\" -O binatang.zip");
    // Nama file zip yang hendak diunzip
    const char *filename = "binatang.zip";

    //membuka file zip
    struct zip *zip_file = zip_open(filename, 0, NULL);
    if (zip_file == NULL) {
        fprintf(stderr, "Error saat membuka file zip!!!\n");
        return 1;
    }

    //unzip setiap gambar yang ada di dalam zip
    int num_entries = zip_get_num_entries(zip_file, 0);
    for (int i = 0; i < num_entries; i++) {
        struct zip_stat file_stat;
        zip_stat_index(zip_file, i, 0, &file_stat);

        char fileOutput[256];
        strcpy(fileOutput, file_stat.name);

        //jika file yang akan diunzip berupa direktori, tidak akan dieksekusi
        if (fileOutput[strlen(fileOutput)-1] == '/') 
        {
            continue;
        }

        struct zip_file *file = zip_fopen_index(zip_file, i, 0);
        if (file == NULL) 
        {
            fprintf(stderr, "Error saat membuka file didalam zip!!!\n");
            zip_close(zip_file);
            return 1;
        }

        FILE *fp = fopen(fileOutput, "wb");
        if (fp == NULL) 
        {
            fprintf(stderr, "Error saat membuat output file!!!\n");
            zip_fclose(file);
            zip_close(zip_file);
            return 1;
        }

        char buffer[4096];
        int n;
        do 
        {
            n = zip_fread(file, buffer, sizeof(buffer));
            if (n > 0) {
                fwrite(buffer, n, 1, fp);
            }
        } while (n > 0);
        fclose(fp);
        zip_fclose(file);
    }
    //menutup file zip
    zip_close(zip_file);
    printf("File sudah diunzip\n");

    //b. pemilihan secara acak
    // membuat array of strings untuk menyimpan nama file yang valid
    const char *valid_extensions[] = {".jpg", ".jpeg", ".png"};
    int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);

    // membuka direktori tempat file di-unzip
    DIR *dir = opendir(".");
    if (dir == NULL) {
        fprintf(stderr, "Error saat membuka direktori\n");
        return 1;
    }

    // menyimpan semua nama file yang valid ke dalam array
    const int MAX_FILES = 1000;
    char filenames[MAX_FILES][256];
    int num_files = 0;
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        char *ext = strrchr(entry->d_name, '.');
        if (ext == NULL) {
            continue;
        }
        for (int i = 0; i < num_valid_extensions; i++) {
            if (strcmp(ext, valid_extensions[i]) == 0) {
                strcpy(filenames[num_files], entry->d_name);
                num_files++;
                break;
            }
        }
    }
    closedir(dir);

    // mengacak nomor indeks file yang akan diambil dari array filenames
    srand(time(NULL));
    int random_index = rand() % num_files;
    char *random_filename = filenames[random_index];

    // mencetak file gambar yang terpilih secara acak
    printf("Gambar yang terpilih secara acak: %s\n", random_filename);


    //c. membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
    system("mkdir -p HewanDarat");
    system("mkdir -p HewanAmphibi");
    system("mkdir -p HewanAir");
    printf("Folder sudah dibuat.\n");

    //memindahkan gambar ke direktori masing masing tempat tinggal
    system("mv *_darat.jpg HewanDarat");
    system("mv *_amphibi.jpg HewanAmphibi");
    system("mv *_air.jpg HewanAir");
    printf("File gambar sudah dipindah.\n");

    char dir1[] = "HewanDarat";
    char dir2[] = "HewanAmphibi";
    char dir3[] = "HewanAir";

    //d. melakukan zip
    zip("HewanDarat.zip", dir1);
    zip("HewanAmphibi.zip", dir2);
    zip("HewanAir.zip", dir3);

    system("rm -r HewanDarat");
    system("rm -r HewanAmphibi");
    system("rm -r HewanAir");
    printf("Folder sudah dihapus.\n");
    return 0;
}

