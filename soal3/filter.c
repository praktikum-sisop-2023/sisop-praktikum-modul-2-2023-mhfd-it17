#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>
#include <stdio.h>

#define ZIP_FILE_NAME "players.zip"
#define EXTRACTED_FOLDER_NAME "players"
#define KIPER "kiper"
#define BEK "bek"
#define GELANDANG "gelandang"
#define PENYERANG "penyerang"
#define URL "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF"  

bool downloadFile() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("wget", "wget", "-q", "-O", ZIP_FILE_NAME, URL, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }

  return false;
}

bool extractZip() {
  pid_t pid;
  int status;

  pid = fork();

  if (pid == 0) {
    execlp("unzip", "unzip", "-q", ZIP_FILE_NAME, "-d", EXTRACTED_FOLDER_NAME, NULL);
  } else if (pid > 0) {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
      return true;
    }
  }
  return false;
}

void buatFolder() {
  char *folderName[] = {"./players/players/Kiper", "./players/players/Bek","./players/players/Gelandang","./players/players/Penyerang"};
  pid_t pid = fork();
  if (pid == 0) {
    for (int i = 0; i < 4; i++) {
      pid_t pid = fork();
      char *args[] = {"mkdir", "-p", folderName[i], NULL};
      if (pid == 0) {
        execvp("mkdir", args);
      } else if (pid > 0) {
        waitpid(pid, NULL, 0);
      }
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}

void hapusNonManUtdPlayers() {
  DIR *dir;
  struct dirent *entry;
  char filename[256], team[256], *p, path[512];
  dir = opendir("./players/players");
  pid_t pid;
  pid = fork();
  if (pid == 0) {
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            if (strcmp(p, "ManUtd") != 0) {
              sprintf(path, "./players/players/%s", entry->d_name);
              remove(path);
            }
          }
        }
      }
      closedir(dir);
    }
  } else if (pid > 0) {
    waitpid(pid, NULL, 0);
    exit(0);
  }
}

void kategoriPlayers() {
  pid_t pid, create_dir;
  struct dirent *entry;

  char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"}, filename[256],
       *p, path[512];
  DIR *dir;

  for (int i = 0; i < 4; i++) {

    pid_t pid;
    pid = fork();
    dir = opendir("./players/players");

    if (pid == 0 && dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          if (p != NULL) {
            sprintf(path, "./players/players/%s", entry->d_name);
            char folder_path[50];
            if (strcmp(positions[i], p) == 0) {
              sprintf(folder_path, "./players/players/%s", positions[i]);
              char *args[] = {"mv", path, folder_path, NULL};
              pid_t cid;
              cid = fork();
              if (cid == 0)
                execvp("mv", args);
              else if (cid > 0) {
                waitpid(cid, NULL, 0);
              }
            }
          }
        }
      }
    } else if (pid > 0) {
      waitpid(pid, NULL, 0);
      exit(0);
    }
  }
}

int compare(const void *a, const void *b) {
    const int *int_a = (const int*)a;
    const int *int_b = (const int*)b;
    
    if (*int_a > *int_b) {
        return -1;
    } else if (*int_a == *int_b) {
        return 0;
    } else {
        return 1;
    }
}

void buatTim(int b, int g, int p) {
  int pemain[3] = {b, g, p};
  char *positions[] = {"./players/players/Bek", "./players/players/Gelandang",
                       "./players/players/Penyerang"},
       filename[256];
  FILE *fp;
  char nama_file[100];
  char *home_dir = getenv("HOME");
  sprintf(nama_file, "%s/Formasi_%d-%d-%d.txt", home_dir, b, g, p);
  fp = fopen(nama_file, "w");
  fputs("./players/Kiper\nDeGea\n", fp);
  for (int i = 0; i < 3; i++) {
    char p[100];
    sprintf(p, "%s\n", positions[i]);
    fputs(p, fp);
    DIR *dir;
    int arr[10];
    struct dirent *entry;
    dir = opendir(positions[i]);
    int j = 0;
    if (dir != NULL) {
      while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          arr[j] = atoi(p);
          j++;
        }
      }
      closedir(dir);
      qsort(arr, j, sizeof(int), compare);
      char strings[20][10];
      for (int l = 0; l < pemain[i]; l++) {
        dir = opendir(positions[i]);
        while ((entry = readdir(dir)) != NULL) {
          strcpy(filename, entry->d_name);
          char *p = strtok(filename, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, "_");
          p = strtok(NULL, ".");
          if (p != NULL && arr[l] == atoi(p) &&
              strcmp(filename, strings[l - 1]) != 0) {
            strcpy(strings[l], filename);
            strcat(filename, "\n");
            fputs(filename, fp);
            break;
          }
        }
      }
    }
  }
}

int main(int argc, char *argv[]) { 

int b;
int g;
int p;

  printf("Downloading zip file...\n");

  if (!downloadFile()) {
    printf("Failed to download zip file!\n");
    return 1;
  }

  printf("Extracting zip file...\n");

  if (!extractZip()) {
    printf("Failed to extract zip file!\n");
    return 1;
  }

  printf("Deleting zip file...\n");

  if (remove(ZIP_FILE_NAME) == 0) {
    printf("Zip file deleted successfully!\n");
  } else {
    printf("Failed to delete zip file!\n");
    return 1;
  }
  printf("Creating positions folder...\n");
  buatFolder();
  printf("Remove Non MU players...\n");
  hapusNonManUtdPlayers();
  printf("Categorize players position...\n");
  kategoriPlayers();
  printf("Make team...\n");
  pid_t pid_tim;
  pid_tim = fork();

  if (pid_tim == 0) {
    printf("Masukkan Jumlah Bek: ");
    scanf("%d", &b);
    printf("Masukkan Jumlah Gelandang: ");
    scanf("%d", &g);
    printf("Masukkan Jumlah Penyerang: ");
    scanf("%d", &p);
    buatTim(b, g, p);

  } else if (pid_tim > 0) {
    waitpid(pid_tim, NULL, 0);
    exit(0);
  }

  return 0;
}
